package in.co.adnan.retailsample.response;

public class AddOrderProductResponse {

	private int userId;
	private Double subTotal;
	private Double discount;
	private Double grandTotal;

	

	public AddOrderProductResponse() {
	}
	
	public AddOrderProductResponse(int userId, Double subTotal, Double discount, Double grandTotal) {
		super();
		this.userId = userId;
		this.subTotal = subTotal;
		this.discount = discount;
		this.grandTotal = grandTotal;
	}


	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	
	

}
