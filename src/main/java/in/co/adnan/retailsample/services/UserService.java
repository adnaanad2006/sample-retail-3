package in.co.adnan.retailsample.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.adnan.retailsample.model.User;
import in.co.adnan.retailsample.model.UserFactory;
import in.co.adnan.retailsample.repos.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	List<User> users = new ArrayList<>();
	
		

	public List<User> getAllUsers() {
		users = userRepository.findAll();
		return users;
	}

	public User addRandomUser(User user) {

		userRepository.save(user);
	    
		return user;
	}

	public User getUserById(int userId) {

		Optional<User> user = userRepository.findById(userId);
		if(user.isPresent()) {
			return UserFactory.getUser(user.get().getType());
		}
		return null;
	}
	
}
