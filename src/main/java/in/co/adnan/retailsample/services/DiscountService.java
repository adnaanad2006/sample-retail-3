package in.co.adnan.retailsample.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.adnan.retailsample.model.Discount;
import in.co.adnan.retailsample.repos.DiscountRepository;

@Service
public class DiscountService {

	@Autowired
	DiscountRepository discountRepository;
	
	
	public Discount getDiscountById(int id) {

		return discountRepository.findById(id).orElse(null);
	}
	
}
