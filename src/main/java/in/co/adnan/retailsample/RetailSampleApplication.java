package in.co.adnan.retailsample;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import in.co.adnan.retailsample.model.Category;
import in.co.adnan.retailsample.model.Discount;
import in.co.adnan.retailsample.model.DiscountFactory;
import in.co.adnan.retailsample.model.User;
import in.co.adnan.retailsample.model.UserFactory;
import in.co.adnan.retailsample.model.UserType;
import in.co.adnan.retailsample.repos.CategoryRepository;
import in.co.adnan.retailsample.repos.DiscountRepository;
import in.co.adnan.retailsample.repos.RoleRepository;
import in.co.adnan.retailsample.services.UserService;

@SpringBootApplication(scanBasePackages = "in.co.adnan.retailsample")
@EntityScan( basePackages = {"in.co.adnan.retailsample"})
public class RetailSampleApplication implements CommandLineRunner {

	@Autowired
	UserService userService;
	@Autowired
	RoleRepository roleRepo;
	@Autowired
	CategoryRepository categoryRepo;
	@Autowired
	DiscountRepository discountRepo;

	public static void main(String[] args) {
		SpringApplication.run(RetailSampleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		
		

		Category grocery = new Category(1, "Grocery");
		Category other = new Category(2, "Other");
		categoryRepo.save(grocery);
		categoryRepo.save(other);
		

		
		User u1 = UserFactory.getUser(UserType.EMPLOYEE);
		userService.addRandomUser(u1);
		User u2 = UserFactory.getUser(UserType.AFFILIATE);
		userService.addRandomUser(u2);
		User u3 = UserFactory.getUser(UserType.CUSTOMER);
		userService.addRandomUser(u3);
		
		

		Discount employeeDiscount = DiscountFactory.getDiscount(u1.getType());
		Discount affiDiscount = DiscountFactory.getDiscount(u2.getType());
		Discount customerDiscount = DiscountFactory.getDiscount(u3.getType());
		Discount fixedDiscount = DiscountFactory.getDiscount(null);

		discountRepo.save(employeeDiscount);
		discountRepo.save(affiDiscount);
		discountRepo.save(customerDiscount);
		discountRepo.save(fixedDiscount);
		
		
	}
	
	

}
