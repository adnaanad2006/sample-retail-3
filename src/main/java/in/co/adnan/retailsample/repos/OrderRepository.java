package in.co.adnan.retailsample.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.adnan.retailsample.model.Order;

public interface OrderRepository extends JpaRepository<Order, Integer> {

}
