package in.co.adnan.retailsample.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.adnan.retailsample.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
