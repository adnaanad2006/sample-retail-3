package in.co.adnan.retailsample.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.adnan.retailsample.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
