package in.co.adnan.retailsample.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.co.adnan.retailsample.model.Cart;
import in.co.adnan.retailsample.model.Discount;
import in.co.adnan.retailsample.model.DiscountFactory;
import in.co.adnan.retailsample.model.Product;
import in.co.adnan.retailsample.model.User;
import in.co.adnan.retailsample.request.AddOrderProductsRequest;
import in.co.adnan.retailsample.response.AddOrderProductResponse;
import in.co.adnan.retailsample.response.BaseResponse;
import in.co.adnan.retailsample.services.DiscountService;
import in.co.adnan.retailsample.services.UserService;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	UserService userService;

	@Autowired
	DiscountService discountService;
	


    /**
     * Add products to cart, apply percentage based and fixed discount according to user. 
     * Calculate total product amount, total discount and grand total. 
     * Returns total product amount, total discount and grand total of the cart.
     * 
     * @author Adnan
     * @param  req  userId and List of products AddOrderProductsRequest
     * @return      total product amount, total discount and grand total of the cart.
     */
	@PostMapping("addToCart")
	public BaseResponse<AddOrderProductResponse> addProducts(@Valid @RequestBody AddOrderProductsRequest req) {

		
		
		int userId = req.getUserId();
		User user = userService.getUserById(userId);
		List<Product> products = req.getProducts();



		Cart cart = new Cart(user);
		
		cart.addAllProduct(products);
		
		/*
		 * Get appropriate percentage discount for the user
		 * 
		 * */
		Discount percentageDiscount = DiscountFactory.getDiscount(user.getType());
		cart.addDiscount(percentageDiscount);

		/*
		 * Get fixed discount for the user
		 * 
		 * */
		Discount fixedDiscount = DiscountFactory.getDiscount(null);
		
		
		cart.addDiscount(fixedDiscount);
		
		cart.process();
		
		BaseResponse<AddOrderProductResponse> response = new BaseResponse<>(1, "Cart Updated successfully", new AddOrderProductResponse());
		
		
		response.getData().setUserId(cart.getUser().getId());
		response.getData().setSubTotal(cart.getSubtotal());
		response.getData().setDiscount(cart.getTotalDiscount());
		response.getData().setGrandTotal(cart.getGrandTotal());
		

		return response;
	    
	}
	
	
}
