package in.co.adnan.retailsample.model;

public interface IDiscount {

	double apply(double original);
}
