package in.co.adnan.retailsample.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;

import in.co.adnan.retailsample.constants.Constants;

@Entity
public class Customer extends User {
	
	Customer() {
		super(UserType.CUSTOMER);
		setDateCreated(new Date());
	}

	public Customer(Date date) {
		super(UserType.CUSTOMER);
		setDateCreated(date);
	}

	@Override
	public boolean isEligibleForPercentageDiscount() {
		
		Date createdDate = getDateCreated();
		Calendar pastDate = Calendar.getInstance();
		pastDate.add(Calendar.MONTH, (Constants.CUTOMER_DISCOUNT_ELLIGIBILITY * -1));

		
		// If user created date is less than past date
		return createdDate.compareTo(pastDate.getTime()) < 0;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

}
