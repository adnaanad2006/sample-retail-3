package in.co.adnan.retailsample.model;

public enum DiscountType {
	PERCENTAGE, FIXED
}
