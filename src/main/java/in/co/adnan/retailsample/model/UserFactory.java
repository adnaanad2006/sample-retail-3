package in.co.adnan.retailsample.model;

import java.util.Date;

public class UserFactory {
	
	private UserFactory() {
		
	}
	
	public static User getUser(UserType type) {
		return getUser(type, new Date());
	}
	
	public static User getUser(UserType type, Date date) {
		User user = null;
		
		switch(type) {
			case EMPLOYEE:
				user = new Employee(date);
				break;
			case AFFILIATE:
				user = new Affiliate(date);
				break;
			case CUSTOMER:
				user = new Customer(date);
				break;
		}
		return user;
	}

}
