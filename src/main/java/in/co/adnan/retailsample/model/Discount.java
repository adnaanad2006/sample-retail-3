package in.co.adnan.retailsample.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "discounts")
public abstract class Discount implements IDiscount {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;
	
	private DiscountType type;
	
	private float discountAmount;

	public Discount() {
		super();
	}

	public Discount(DiscountType type) {
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public DiscountType getType() {
		return type;
	}

	public void setType(DiscountType type) {
		this.type = type;
	}

	public float getDiscount() {
		return discountAmount;
	}

	public void setDiscount(float discount) {
		this.discountAmount = discount;
	}

	@Override
	public String toString() {
		return "Discount [id=" + id + ", name=" + name + ", discountType=" + type + ", discount=" + discountAmount
				+ "]";
	}
	
	
	
	
	
	
}
