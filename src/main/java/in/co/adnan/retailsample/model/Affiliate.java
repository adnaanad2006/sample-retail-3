package in.co.adnan.retailsample.model;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class Affiliate extends User {
	
	public Affiliate() {
		super(UserType.AFFILIATE);
		setDateCreated(new Date());
	}

	public Affiliate(Date date) {
		super(UserType.AFFILIATE);
		setDateCreated(date);
	}

	@Override
	public boolean isEligibleForPercentageDiscount() {
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

}
