package in.co.adnan.retailsample.model;

public class DiscountFactory {
	
	private DiscountFactory() {
		
	}
	
	public static Discount getDiscount(UserType type) {
		
		if(type == null) {
			return new Discount5Per100();
			
		}

		Discount discount = null;
		
		switch(type) {
			case EMPLOYEE:
				discount = new Discount30Percent();
				break;
			case AFFILIATE:
				discount = new Discount10Percent();
				break;
			case CUSTOMER:
				discount = new Discount5Percent();
				break;
		}
		
		return discount;
		
	}

}
