package in.co.adnan.retailsample.model;

import javax.persistence.Entity;

@Entity
public class Discount5Per100 extends Discount {

	public Discount5Per100() {
		super(DiscountType.FIXED);
		setName("5 Percent 100");
		setDiscount(5.0f);
	}
	

	@Override
	public double apply(double original) {
		return (( Math.floor(original / 100) ) * getDiscount());
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
	
	
	
	
	
}
