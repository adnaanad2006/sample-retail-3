package in.co.adnan.retailsample.model;

import javax.persistence.Entity;

@Entity
public class Discount5Percent extends Discount {

	public Discount5Percent() {
		super(DiscountType.PERCENTAGE);
		setName("5 Percent Discount");
		setDiscount(5.0f);
	}

	

	@Override
	public double apply(double original) {
		return (original * getDiscount() / 100);
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
	
	
	
	
	
}
