package in.co.adnan.retailsample.model;

import javax.persistence.Entity;

@Entity
public class Discount30Percent extends Discount {

	public Discount30Percent() {
		super(DiscountType.PERCENTAGE);
		setName("30 Percent Discount");
		setDiscount(30.0f);
	}
	

	@Override
	public double apply(double original) {
		return (original * getDiscount() / 100);
	}

	@Override
	public String toString() {
		return super.toString();
	}
	
	
	
	
	
	
}
