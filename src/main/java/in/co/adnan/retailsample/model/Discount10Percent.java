package in.co.adnan.retailsample.model;

import javax.persistence.Entity;

@Entity
public class Discount10Percent extends Discount {

	public Discount10Percent() {
		super(DiscountType.PERCENTAGE);
		setName("10 Percent Discount");
		setDiscount(10.0f);
	}

	

	@Override
	public double apply(double original) {
		return (original * getDiscount() / 100);
	}



	@Override
	public String toString() {
		return super.toString();
	}
	
	
	
	
	
	
}
