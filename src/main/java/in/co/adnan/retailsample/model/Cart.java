package in.co.adnan.retailsample.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Cart extends BaseCart {

	
	public Cart(User user) {
		super(user);
		products = new ArrayList<>();
		discounts = new ArrayList<>();
	}
	
	private int id;
	private List<Product> products;
	private List<Discount> discounts;
	private double subtotal;
	private double totalDiscount;
	private double grandTotal;

	@Override
	public void addDiscount(Discount discount) {
		/*
		 * Add fixed discount
		 * Add percentage discount if user is eligible for that 
		 * */
		if(discount.getType().equals(DiscountType.FIXED) || 
				(discount.getType().equals(DiscountType.PERCENTAGE) && user.isEligibleForPercentageDiscount())) {
			this.discounts.add(discount);
		}
	}

	
	@Override
	public void addProduct(Product product) {
		this.products.add(product);
	}

	@Override
	public void addAllProduct(List<Product> products) {
		this.products.addAll(products);
	}

	@Override
	public boolean hasGrocery() {

		List<Product> groceryProducts = products.stream().filter(p ->  p.getCategory().getName().equals("Grocery") ).collect(Collectors.toList());

		return !groceryProducts.isEmpty();
	}

	@Override
	public void calculateSubTotal() {
		this.subtotal = products.stream().mapToDouble(p ->  p.getPrice()*p.getQuantity() ).sum();
		
	}

	@Override
	public void applyAllDiscount() {
		discounts.forEach(discount -> { 
			if((discount.getType().equals(DiscountType.PERCENTAGE) && !hasGrocery()) || discount.getType().equals(DiscountType.FIXED)) {
				totalDiscount += discount.apply(subtotal);
			} 
			
			if(totalDiscount > subtotal ) {
				totalDiscount = subtotal;
			}
			 
			} );
	}

	@Override
	public void calculateGrandTotal() {
		
		grandTotal = subtotal - totalDiscount;
		
	}


	public List<Product> getProducts() {
		return products;
	}


	public List<Discount> getDiscounts() {
		return discounts;
	}


	public double getSubtotal() {
		return subtotal;
	}


	public double getTotalDiscount() {
		return totalDiscount;
	}


	public double getGrandTotal() {
		return grandTotal;
	}

	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	@Override
	public String toString() {
		return "Cart [products=" + products + ", discounts=" + discounts + ", subtotal=" + subtotal + ", totalDiscount="
				+ totalDiscount + ", grandTotal=" + grandTotal + "]";
	}
	
	

}
