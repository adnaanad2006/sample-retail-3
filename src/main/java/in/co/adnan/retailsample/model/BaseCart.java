package in.co.adnan.retailsample.model;

import java.util.List;

public abstract class BaseCart {
	

	public final User user;
	
	

	public BaseCart(User user) {
		this.user = user;
	}
	
	public abstract void addProduct(Product product);
	public abstract void addAllProduct(List<Product> products);
	public abstract void addDiscount(Discount discount);
	public abstract boolean hasGrocery();
	public abstract void calculateSubTotal();
	public abstract void applyAllDiscount();
	public abstract void calculateGrandTotal();
	
	
	public final void  process() {
		
		calculateSubTotal();
		applyAllDiscount();
		calculateGrandTotal();
		
		
	}

	public User getUser() {
		return user;
	}
	
	
	
	
}
