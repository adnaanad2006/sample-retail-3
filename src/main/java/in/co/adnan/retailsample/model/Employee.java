package in.co.adnan.retailsample.model;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class Employee extends User {

	public Employee() {
		super(UserType.EMPLOYEE);
		setDateCreated(new Date());
	}
	
	public Employee(Date date) {
		super(UserType.EMPLOYEE);
		setDateCreated(date);
	}

	@Override
	public boolean isEligibleForPercentageDiscount() {
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
	

}
