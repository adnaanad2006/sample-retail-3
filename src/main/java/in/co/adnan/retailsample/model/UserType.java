package in.co.adnan.retailsample.model;

public enum UserType {
	EMPLOYEE, AFFILIATE, CUSTOMER
}
