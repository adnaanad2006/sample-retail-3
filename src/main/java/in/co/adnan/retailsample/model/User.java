package in.co.adnan.retailsample.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;


@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	

	private String name;
	
	private UserType type;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    @CreatedDate
    private Date dateCreated;
    
	public User() {
		super();
	}

	


	

	public User(UserType type) {
		super();
		this.type = type;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public boolean isEmployee() {
		return type.equals(UserType.EMPLOYEE);
	}
	
	public boolean isAffiliate() {
		return type.equals(UserType.AFFILIATE);
	}
	
	public boolean isCustomer() {
		return type.equals(UserType.CUSTOMER);
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}


	public boolean isEligibleForPercentageDiscount() {
		return false;
	}






	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", type=" + type + ", dateCreated=" + dateCreated + "]";
	}
	
	
	
	
	

}
