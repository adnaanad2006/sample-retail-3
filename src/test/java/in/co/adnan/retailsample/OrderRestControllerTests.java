package in.co.adnan.retailsample;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.co.adnan.retailsample.controller.OrderController;
import in.co.adnan.retailsample.model.Category;
import in.co.adnan.retailsample.model.Discount;
import in.co.adnan.retailsample.model.Product;
import in.co.adnan.retailsample.model.User;
import in.co.adnan.retailsample.model.UserFactory;
import in.co.adnan.retailsample.model.UserType;
import in.co.adnan.retailsample.repos.CategoryRepository;
import in.co.adnan.retailsample.repos.DiscountRepository;
import in.co.adnan.retailsample.repos.RoleRepository;
import in.co.adnan.retailsample.request.AddOrderProductsRequest;
import in.co.adnan.retailsample.services.DiscountService;
import in.co.adnan.retailsample.services.UserService;


@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderRestControllerTests {

	@Autowired
	private MockMvc mvc;
	
	private static int initialized = 0;
	

    @MockBean
    private UserService userService;
    @MockBean
    private DiscountService discountService;

    @MockBean
	private RoleRepository roleRepo;

    @MockBean
	private CategoryRepository categoryRepo;

    @MockBean
	private DiscountRepository discountRepo;
    
    final String ADD_TO_CART = "/orders/addToCart";

    private static User userEmployee;
    private static User userAffiliate;
    private static User userCustomer;
    private static User userCustomerOld;
    

    @BeforeClass
    public static void initialize() {
    	
    	userEmployee = UserFactory.getUser(UserType.EMPLOYEE);
    	userAffiliate = UserFactory.getUser(UserType.AFFILIATE);
    	userCustomer = UserFactory.getUser(UserType.CUSTOMER);

		
		Calendar past2Year = Calendar.getInstance();
		past2Year.add(Calendar.MONTH, -25);

		userCustomerOld = UserFactory.getUser(UserType.CUSTOMER, past2Year.getTime());
		
    }
    


    /**
     * Check the employee user discount value, having total amount 85 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withEmployee_withTotal85_thenReturnJsonArray() throws Exception {
		
		int userId = 1;

	    Mockito.when(userService.getUserById(userId)).thenReturn(userEmployee);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct85());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(85.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(25.5))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(59.5));
	}

    


    /**
     * Check the employee user discount value, having total amount 515 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withEmployee_withTotal515_thenReturnJsonArray() throws Exception {
		
		int userId = 1;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userEmployee);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct515());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(515.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(179.5))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(335.5));
	}

    


    /**
     * Check the employee user discount value, having total amount 995 for grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withEmployee_withTotal995_withGroceryProduct_thenReturnJsonArray() throws Exception {
		
		int userId = 1;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userEmployee);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyGroceryProduct995());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(995.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(45.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(950.0));
	}

    


    /**
     * Check the employee user discount value, having total amount 995 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withEmployee_withTotal995_thenReturnJsonArray() throws Exception {
		
		int userId = 1;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userEmployee);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct995());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(995.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(343.5))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(651.5));
	}

    


    /**
     * Check the affiliate user discount value, having total amount 85 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withAffiliate_withTotal85_thenReturnJsonArray() throws Exception {
		
		int userId = 2;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userAffiliate);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct85());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(85.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(8.5))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(76.5));
	}

    

    /**
     * Check the affiliate user discount value, having total amount 515 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withAffiliate_withTotal515_thenReturnJsonArray() throws Exception {
		
		int userId = 2;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userAffiliate);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct515());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(515.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(76.5))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(438.5));
	}

    

    /**
     * Check the affiliate user discount value, having total amount 995 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withAffiliate_withTotal995_thenReturnJsonArray() throws Exception {
		
		int userId = 2;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userAffiliate);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct995());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(995.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(144.5))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(850.5));
	}

    

    /**
     * Check the customer user discount value, having total amount 85 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withCustomer_withTotal85_thenReturnJsonArray() throws Exception {
		
		int userId = 3;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userCustomer);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct85());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(85.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(0.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(85.0));
	}

    
    

    /**
     * Check the customer user discount value, having total amount 515 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withCustomer_withTotal515_thenReturnJsonArray() throws Exception {
		
		int userId = 3;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userCustomer);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct515());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(515.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(25.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(490.0));
	}

    
    

    /**
     * Check the customer user discount value, having total amount 995 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withCustomer_withTotal995_thenReturnJsonArray() throws Exception {
		
		int userId = 3;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userCustomer);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct995());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(995.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(45.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(950.0));
	}
    
    

    /**
     * Check customer user discount value which is the more than 2 years old.
     * Customer having total amount 85 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withOldCustomer_withTotal85_thenReturnJsonArray() throws Exception {
		
		int userId = 3;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userCustomerOld);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct85());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(85.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(4.25))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(80.75));
	}

    
    

    /**
     * Check customer user discount value which is the more than 2 years old.
     * Customer having total amount 515 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withOldCustomer_withTotal515_thenReturnJsonArray() throws Exception {
		
		int userId = 3;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userCustomerOld);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct515());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(515.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(50.75))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(464.25));
	}

    
    

    /**
     * Check customer user discount value which is the more than 2 years old.
     * Customer having total amount 995 for non grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withOldCustomer_withTotal995_thenReturnJsonArray() throws Exception {
		
		int userId = 3;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userCustomerOld);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyProduct995());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(995.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(94.75))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(900.25));
	}
	


    
    

    /**
     * Check customer user discount value which is the more than 2 years old.
     * Customer having total amount 995 for grocery products.
     * 
     * @return void
     */
	@Test
	public void addOrderApi_withOldCustomer_withTotal995_withGrocery_thenReturnJsonArray() throws Exception {
		
		int userId = 3;
	 
	    Mockito.when(userService.getUserById(userId)).thenReturn(userCustomerOld);	
		
		
		AddOrderProductsRequest aorp = new AddOrderProductsRequest(userId, 1, getDummyGroceryProduct995());
		
		mvc.perform( MockMvcRequestBuilders
			      .post(ADD_TO_CART)
			      .content(asJsonString(aorp))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.status").value(1))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data").exists())
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.subTotal").value(995.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.discount").value(45.0))
			      .andExpect(MockMvcResultMatchers.jsonPath("$.data.grandTotal").value(950.00));
	}
	
	
		 
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
	
	
	
	
	private List<Product> getDummyProduct85() {
	
		List<Product> productList85 = new ArrayList<>(); // Total Cart Amount : 85
		productList85.add(new Product(0, "P1", 5.00, 6, getOtherCategory())); // 30
		productList85.add(new Product(0, "P2", 10.00, 2, getOtherCategory())); // 20
		productList85.add(new Product(0, "P3", 8.00, 1, getOtherCategory())); // 8
		productList85.add(new Product(0, "P4", 9.00, 3, getOtherCategory())); // 27
		
		return productList85;
	}
	
	
	private List<Product> getDummyProduct515() {
	
		List<Product> productList515 = new ArrayList<>(); // Total Cart Amount : 515
		productList515.add(new Product(0, "P1", 50.00, 6, getOtherCategory())); // 300
		productList515.add(new Product(0, "P2", 25.00, 2, getOtherCategory())); // 50
		productList515.add(new Product(0, "P3", 20.00, 6, getOtherCategory())); // 120
		productList515.add(new Product(0, "P4", 15.00, 3, getOtherCategory())); // 45
		return productList515;
	}
	
	
	private List<Product> getDummyProduct995() {
	
		List<Product> productList995 = new ArrayList<>(); // Total Cart Amount : 995

		productList995.add(new Product(0, "P1", 30.00, 6, getOtherCategory())); // 180
		productList995.add(new Product(0, "P2", 70.00, 2, getOtherCategory())); // 140
		productList995.add(new Product(0, "P3", 80.00, 6, getOtherCategory())); // 480
		productList995.add(new Product(0, "P4", 65.00, 3, getOtherCategory())); // 195
		
		return productList995;
	}
	
	
	private List<Product> getDummyGroceryProduct995() {
	
		List<Product> productList995 = new ArrayList<>(); // Total Cart Amount : 995

		productList995.add(new Product(0, "P1", 30.00, 6, getOtherCategory())); // 180
		productList995.add(new Product(0, "P2", 70.00, 2, getGroceryCategory())); // 140
		productList995.add(new Product(0, "P3", 80.00, 6, getOtherCategory())); // 480
		productList995.add(new Product(0, "P4", 65.00, 3, getOtherCategory())); // 195
		
		return productList995;
	}
	
	private Category getGroceryCategory() {
		return new Category(1, "Grocery");
	}
	
	private Category getOtherCategory() {
		return new Category(2, "Other");
	}
	

	
}
